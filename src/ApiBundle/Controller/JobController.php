<?php

namespace ApiBundle\Controller;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;

class JobController extends FOSRestController
{
    /**
     * @Rest\View
     */
    public function getJobsAction($token)
    {
        $em = $this->getDoctrine()->getManager();
        $affiliate = $em->getRepository('AppBundle:Affiliate')->findOneByToken($token);

        if (!$affiliate || !$affiliate->getIsActive()) {
            throw $this->createNotFoundException();
        }

        // $jobs = $affiliate->getActiveJobs();
        // $jobs = $em->getRepository('AppBundle:Job')->getActiveJobsForAffiliate($affiliate)); // main
        $jobs = $em->getRepository('AppBundle:Job')->getActiveJobsForAffiliate($affiliate->getId());

        $view = $this->view($jobs);

        return $this->handleView($view);
    }
}
